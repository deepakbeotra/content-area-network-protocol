/**filename Client.java
 *
 * @Version $Id: Client.java,v 1.0 2013/03/17 19:07:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.io.Serializable;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

/**
 * This is the Client class that extends UnicastRemoteObject and implements
 * MiniServerInterface,Serializable.
 * 
 * @author1 Deepak Mahajan
 * 
 */

public class Client extends UnicastRemoteObject implements MiniServerInterface,
Serializable {

	/**
	 * Default Constructor
	 * 
	 * @throws RemoteException
	 */
	protected Client() throws RemoteException {
		super();

	}

	// Object of Peer1 class
	static Peer1 me;
	// Object of BootServerInterface
	static BootServerInterface server;

	/**
	 * This is the main method
	 * 
	 * @param args
	 * @throws RemoteException
	 * @throws AlreadyBoundException
	 * @throws NotBoundException
	 */
	public static void main(String[] args) throws RemoteException,
	AlreadyBoundException, NotBoundException {

		// looking for server's object
		Registry reg2 = LocateRegistry.getRegistry("129.21.30.38", 12345);
		// System.out.println("hello again");

		String value = null;
		Boolean connected = false;
		Scanner sc = new Scanner(System.in);
		// // Mini Server ////
		MiniServerInterface ms1 = new Client();
		Registry reg1 = LocateRegistry.createRegistry(12888);
		// binding self object on registry and acting as a miniserver
		reg1.rebind("ms1", ms1);
		// System.out.println("miniserver created");
		// System.out.println("after creation of object p");
		System.out.println("Enter join");
		while (true) {
			value = sc.next();
			if (value.equalsIgnoreCase("join") && connected == false) {
				server = (BootServerInterface) reg2.lookup("server1");
				// System.out.println("connection created in client side");
				me = new Peer1(server);
				// Calculates the random x and y cooordinate od the peer to
				// route
				Double x = me.calculateCoordinate();
				Double y = me.calculateCoordinate();
				// System.out.println("values returned");
				System.out.println("coordinates " + x + " " + y);

				// bootstraping node
				Peer1 sPeer = server.join(me);
				me.position(me, x, y, sPeer);
				connected = true;
				System.out.println("I am created and My Credentials are Name: "
						+ me.name + "  X " + me.x + " Y " + me.y + " LeftX "
						+ me.lx + " RightX " + me.rx + " LeftY " + me.ly
						+ " RightY " + me.ry);
				System.out.println();
				System.out.print("Neighbours  ");
				for (int i = 0; i < me.neighbours.size(); i++) {
					System.out.print(me.neighbours.get(i).name + " ");
				}
				System.out.println();
				System.out.println("Files");

				// Prints the file name present in the peer.
				for (int i = 0; i < me.files.size(); i++) {
					System.out.print(me.files.get(i).keyword + " ");
				}
			} else if (value.equalsIgnoreCase("join") && connected == true) {
				System.out.println("Already Connected");
			}
			// Inserts the File Name in the peer.
			else if (value.equalsIgnoreCase("insert") && connected == true) {
				System.out.println("Enter File Name");
				String keyword = sc.next();
				String insert = me.insert(keyword);
				System.out.println(insert);
			} else if (value.equalsIgnoreCase("insert") && connected == false) {
				System.out
				.println("You are not connected to system.... Can't Insert");
			}
			// For searching the file name in the peer
			else if (value.equalsIgnoreCase("search") && connected == true) {
				Boolean fileFound = false;
				System.out.println("Enter File Name");
				String keyword = sc.next();
				Peer1 destinyPeer = me.search(keyword);
				for (int i = 0; i < destinyPeer.files.size(); i++) {
					if (destinyPeer.files.get(i).keyword.equals(keyword)) {
						fileFound = true;
						break;
					}
				}
				if (fileFound) {
					System.out.println("File \'" + keyword + "\' Found in "
							+ destinyPeer.name + "  with Ip Address:"
							+ destinyPeer.ip);

				} else {
					System.out.println("Failure");
				}

			}
			// To leave the can system
			else if (value.equalsIgnoreCase("leave") && connected == true) {
				String result = me.removeMe();
				if (result.equals("Can't leave from the System")) {
					System.out.println(result);
				} else {
					System.out.println(result);
					connected = false;
				}
			} else if (value.equalsIgnoreCase("leave") && connected == false) {
				System.out.println("You are already out of the system");
			} else if (value.equalsIgnoreCase("search") && connected == false) {
				System.out
				.println("You are not connected to system.... Can't Search");
			} else if (value.equalsIgnoreCase("view") && connected == true) {
				System.out.println(" My Credentials are Name: " + me.name
						+ "  X " + me.x + " Y " + me.y + " LeftX " + me.lx
						+ " RightX " + me.rx + " LeftY " + me.ly + " RightY "
						+ me.ry);
				System.out.println();
				System.out.print("My Neighbours are ");

				// Prints the name of neighbors of the peer
				for (int i = 0; i < me.neighbours.size(); i++) {
					System.out.print(me.neighbours.get(i).name + " ");
				}
				System.out.println();
				System.out.print("My Files are ");

				// Prints the file name present in the peer.
				for (int i = 0; i < me.files.size(); i++) {
					System.out.print(me.files.get(i).keyword + " ");
				}
				System.out.println();

			} else if (value.equalsIgnoreCase("view") && connected == false) {
				System.out.println("You are not connected to system.... ");
			} else {
				System.out.println("Wrong Input");
				System.out
				.println("Valid Inputs are: 1.join /n 2.view /n 3.insert /n 4.search 5.leave");
			}

		}
	}

	/**
	 * THis method is called remotely for routing during insertion
	 * 
	 * @param file
	 * @throws RemoteException
	 */
	public String insertRouting(File file) throws RemoteException {
		return me.insert(file);
	}

	/**
	 * This method is called remotely for routing during searching.
	 * 
	 * @param file
	 * @throws RemoteException
	 */
	public Peer1 searchRouting(File file) throws RemoteException {
		return me.search(file);
	}

	/**
	 * This method divides the area of the peer into two parts and returns the
	 * new peer.
	 * 
	 * @param X
	 * @param Y
	 * @param p
	 * @throws RemoteException
	 */
	public Peer1 divideArea(Double X, Double Y, Peer1 p) throws RemoteException {
		return me.look(X, Y, p);
	}

	/**
	 * This method calls the neighbor remotely and update the neighbors
	 * credentials.
	 * 
	 * @param p
	 * @param i
	 * @throws RemoteException
	 */
	public void updateNeighbour(Peer1 p, Integer i) throws RemoteException {
		// System.out.println("request recieved to update neighbours  by peer "+
		// p.name);
		if (i.equals(1)) {
			// System.out.println("request to remove me "+p.name);
			me.removeOneNeighbour(p);
		} else if (i.equals(2)) {
			// System.out.println("request to add me "+p.name);
			me.addOneNeighnour(p);
		} else if (i.equals(3)) {
			me.fixNeighbours(p);
		} else {
			System.out.println("Invalid");
		}

	}

	/**
	 * This method is called remotely to download file between two peers.
	 * 
	 * @param fileName
	 * @throws RemoteException
	 */
	public void downLoadFile(String fileName) throws RemoteException {

	}

	/**
	 * This method updates and removes the peer. This method is called remotely.
	 * 
	 * @param up
	 * @param rm
	 * @throws RemoteException
	 */
	public void upAndRm(Peer1 up, Peer1 rm) throws RemoteException {
		me.removeOneNeighbour(rm);
		me.upAndAddNeighbour(up);
	}

	/**
	 * This method returns the name of peer which occupies the space of the
	 * leaving peer.
	 * 
	 * @param p
	 * @param direction
	 * @throws RemoteException
	 */
	public String overTake(Peer1 p, String direction) throws RemoteException {
		return me.occupyPeer(p, direction);
	}
}
