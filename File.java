/**filename File.java
 *
 * @Version $Id: File.java,v 1.0 2013/03/17 16:54:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.io.Serializable;

/**
 * This is the File class that implements Serializable.
 * 
 * @author1 Deepak Mahajan
 * 
 */

public class File implements Serializable {
	// Keyword is initialized.
	String keyword;
	Double X;
	Double Y;

	// Default Constructor
	File() {
		// Default value to keyword
		keyword = null;
		// Default value to x coordinate of keyword
		X = 0.0;
		// Default value to y coordinate of keyword
		Y = 0.0;
	}
}
