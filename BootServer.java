/**filename BootServer.java
 *
 * @Version $Id: BootServer.java,v 1.0 2013/03/17 11:11:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

/**
 * This is the BootServer class that extends UnicastRemoteObjects and implements
 * BootServerInterface
 * 
 * @author1 Deepak Mahajan
 * 
 */
public class BootServer extends UnicastRemoteObject implements
		BootServerInterface {

	// Stores the Boot peer
	public ArrayList<Peer1> al = new ArrayList<Peer1>();
	Integer i = 0;

	public BootServer() throws RemoteException {
		super();

		// TODO Auto-generated constructor stub
	}

	/**
	 * This is the main method.
	 * 
	 * @param args
	 * @throws RemoteException
	 * @throws AlreadyBoundException
	 * @throws NotBoundException
	 */
	public static void main(String args[]) throws RemoteException,
			AlreadyBoundException, NotBoundException {

		BootServerInterface server = new BootServer();
		// Registry is created with port number 12345 for the
		// object of server
		Registry reg1 = LocateRegistry.createRegistry(12345);

		// Binds the registry for the object of server
		reg1.rebind("server1", server);
		System.out.println("server created... registry bound");

	}

	/**
	 * This method sets the name of every peer connected to Boot Server.
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public String setName() throws RemoteException {
		i++;
		return "P" + i;
	}

	/**
	 * This method updates the Boot peer.
	 * 
	 * @param p
	 * @throws RemoteException
	 */
	public void updateNode(Peer1 p) throws RemoteException {
		if (al.size() > 0) {
			if (al.get(0).ip.equals(p.ip)) {
				System.out
						.println("credentials of boot node is updated in server");
				// Removes the peer
				al.remove(0);
				// Adds the peer
				al.add(p);
				System.out.print("Boot Node Credentials are  Ip" + al.get(0).ip
						+ " LeftX " + al.get(0).lx + " LeftY" + al.get(0).ly
						+ "  RightX" + al.get(0).rx + " RightY" + al.get(0).ry
						+ "     X  " + al.get(0).x + "  Y" + al.get(0).y);
				System.out.println();
				System.out.print("Neighbours :");
				for (int i = 0; i < al.get(0).neighbours.size(); i++) {
					System.out.print(al.get(0).neighbours.get(i).name);
				}
				System.out.println();
				System.out.print("Files:");
				for (int i = 0; i < al.get(0).files.size(); i++) {
					System.out.print(al.get(0).files.get(i).keyword);
				}
			}
		}

	}

	/**
	 * This method returns the Boot peer to the client from the server.
	 * 
	 * @throws RemoteException
	 */
	public Peer1 join(Peer1 sP) throws RemoteException {
		if (al.size() == 0) {
			// Adds the peer
			al.add(sP);
			System.out.println("node joined " + sP.ip + "  and same boot node");
			return al.get(0);
		} else {
			System.out.println("node joined " + sP.ip);
			return al.get(0);

		}
	}

	/**
	 * This method changes the Boot peer if Boot peer leaves the system
	 * 
	 * @param oldP
	 * @param newP
	 * @throws RemoteException
	 */
	public void changeNode(Peer1 oldP, Peer1 newP) throws RemoteException {
		if (al.get(0).ip.equals(oldP.ip)) {
			al.remove(0);
			al.add(newP);
			System.out.print("Boot Node Credentials are  Ip" + al.get(0).ip
					+ " LeftX " + al.get(0).lx + " LeftY" + al.get(0).ly
					+ "  RightX" + al.get(0).rx + " RightY" + al.get(0).ry
					+ "     X  " + al.get(0).x + "  Y" + al.get(0).y);
			System.out.println();
			System.out.print("Neighbours :");
			for (int i = 0; i < al.get(0).neighbours.size(); i++) {
				System.out.print(al.get(0).neighbours.get(i).name);
			}
			System.out.println();
			System.out.print("Files:");
			for (int i = 0; i < al.get(0).files.size(); i++) {
				System.out.print(al.get(0).files.get(i).keyword);
			}
		}

	}
}
