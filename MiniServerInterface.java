/**filename MiniServerInterface.java
 *
 * @Version $Id: MiniServerInterface.java,v 1.0 2013/03/17 14:18:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This is the Mini Server interface that extends Remote to make direct
 * communication between peers.
 * 
 * @author1 Deepak Mahajan
 * 
 */
public interface MiniServerInterface extends Remote {

	/**
	 * This method updates and removes the peer. This method is called remotely.
	 * 
	 * @param up
	 * @param rm
	 * @throws RemoteException
	 */
	public void upAndRm(Peer1 up, Peer1 rm) throws RemoteException;

	/**
	 * This method returns the name of peer which occupies the space of the
	 * leaving peer.
	 * 
	 * @param p
	 * @param direction
	 * @throws RemoteException
	 */
	public String overTake(Peer1 p, String direction) throws RemoteException;

	/**
	 * This method divides the area of the peer into two parts and returns the
	 * new peer.
	 * 
	 * @param X
	 * @param Y
	 * @param p
	 * @throws RemoteException
	 */
	public Peer1 divideArea(Double X, Double Y, Peer1 p) throws RemoteException;

	/**
	 * This method calls the neighbor remotely and update the neighbors
	 * credentials.
	 * 
	 * @param p
	 * @param i
	 * @throws RemoteException
	 */
	public void updateNeighbour(Peer1 p, Integer i) throws RemoteException;

	/**
	 * THis method is called remotely for routing during insertion
	 * 
	 * @param file
	 * @throws RemoteException
	 */
	public String insertRouting(File file) throws RemoteException;

	/**
	 * This method is called remotely for routing during searching.
	 * 
	 * @param file
	 * @throws RemoteException
	 */
	public Peer1 searchRouting(File file) throws RemoteException;

	/**
	 * This method is called remotely to download file between two peers.
	 * 
	 * @param fileName
	 * @throws RemoteException
	 */
	public void downLoadFile(String fileName) throws RemoteException;

}
