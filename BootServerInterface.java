/**filename BootServerInterface.java
 *
 * @Version $Id: BootServerInterface.java,v 1.0 2013/03/17 13:17:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This is the BootServer interface that extends Remote
 * 
 * @author1 Deepak Mahajan
 * 
 */
public interface BootServerInterface extends Remote {
	/**
	 * This method returns the Boot peer to the client from the server
	 * 
	 * @param sPeer
	 * @throws RemoteException
	 */
	public Peer1 join(Peer1 sPeer) throws RemoteException;

	/**
	 * This method updates the Boot peer.
	 * 
	 * @param p
	 * @throws RemoteException
	 */
	public void updateNode(Peer1 p) throws RemoteException;

	/**
	 * This method sets the name of every peer connected to Boot Server.
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public String setName() throws RemoteException;

	/**
	 * This method changes the Boot peer if Boot peer leaves the system
	 * 
	 * @param oldP
	 * @param newP
	 * @throws RemoteException
	 */
	public void changeNode(Peer1 oldP, Peer1 newP) throws RemoteException;

}
