/**filename Client.java
 *
 * @Version $Id: Client.java,v 1.0 2013/03/17 19:07:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Random;

/**
 * This is the Peer1 class that implements Serializable.
 * 
 * @author1 Deepak Mahajan
 * 
 */
public class Peer1 implements Serializable
{
	// Peer name is initialized
	String name;
	// ip address string is initialized
	String ip;
	// ArrayList stores the neighbors of peer
	ArrayList<Peer1> neighbours;
	// ArrayList that stores the files of peer
	ArrayList<File> files;
	// Mid x coordinate of peer
	Double x;
	// Mid y coordinate of peer
	Double y;
	// Left x coordinate of peer
	Double lx;
	// Left y coordinate of peer
	Double ly;
	// Right x coordinate of peer
	Double rx;
	// Right y coordinate of peer
	Double ry;
	// Reference variable for the BootServer object
	BootServerInterface s1;
	// Reference variable to store the boot node.
	Peer1 bootNode;
	
	/**
	 * Default constructor
	 * 
	 */
	public Peer1()
	{
		
	}
	
	/**
	 * Parametrized constructor
	 * 
	 * @param s BootServer object
	 */
	public Peer1(BootServerInterface s)	
	{	
		neighbours= new ArrayList<Peer1>();
		files=new ArrayList<File>();
		ip=getIpAddress();
		System.out.println("Current IP address : " + ip);
		
		x=0.0;
		y=0.0;
		lx=0.0;
		ly=0.0;
		rx=0.0;
		ry=0.0;
		s1=s;
			
		try {
			name=s1.setName();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}	
	
	/**
	 * This method occupies the area and credentials of the leaving peer
	 * 
	 * @param p
	 * @param direction
	 * @return
	 */
	public String occupyPeer(Peer1 p,String direction)
	{
		if(direction.equals("vertical"))
		{
			// Expanded vertically
			System.out.println("vertical expansion");
			if(this.ry.equals(p.ly))
			{
				this.ry=p.ry;
				this.y=(this.ly+this.ry)/2;
				this.occupyFiles(p);
				this.occupyNeighbours(p);
				try {
					s1.changeNode(p,this);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("I am expanded vertically and My Credentials are Name: "+name+"  X "+x +" Y "+ y+
						" LeftX "+lx+" RightX "+rx+" LeftY "+ly+" RightY "+ry);
				System.out.println();
				System.out.print("My Neighbours are ");
				for(int i=0;i<neighbours.size();i++)
				{
					System.out.print(neighbours.get(i).name+" ");
				}
				System.out.println();
				System.out.print("My Files are ");
				for(int i=0;i<files.size();i++)
				{
					System.out.print(files.get(i).keyword+" ");
				}	
				System.out.println();
				return "SuccessFully left from system and space is Occupied by Peer: "+this.name +"  with IpAddress: "+this.ip;
				
			}
			else if(this.ly.equals(p.ry))
			{
				//System.out.println( " v   e    elseif");
				this.ly=p.ly;
				this.y=(this.ly+this.ry)/2;
				this.occupyFiles(p);
				this.occupyNeighbours(p);
				try {
					s1.changeNode(p,this);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("I am expanded vertically and My Credentials are Name: "+name+"  X "+x +" Y "+ y+
						" LeftX "+lx+" RightX "+rx+" LeftY "+ly+" RightY "+ry);
				System.out.println();
				System.out.print("My Neighbours are ");
				for(int i=0;i<neighbours.size();i++)
				{
					System.out.print(neighbours.get(i).name+" ");
				}
				System.out.println();
				System.out.print("My Files are ");
				for(int i=0;i<files.size();i++)
				{
					System.out.print(files.get(i).keyword+" ");
				}	
				System.out.println();
				return "SuccessFully left from system and space is Occupied by Peer: "+this.name +"  with IpAddress: "+this.ip;
			}
		}
		// Expanded horizontally
		else if(direction.equals("horizontal"))
		{
			System.out.println("horizontal expansion");
			if(this.rx.equals(p.lx))
			{
			//	System.out.println( " h   e    if");
				this.rx=p.rx;
				this.x=(this.lx+this.rx)/2;
				this.occupyFiles(p);
				this.occupyNeighbours(p);
				try {
					s1.changeNode(p,this);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("I am expanded Horizontally and My Credentials are Name: "+name+"  X "+x +" Y "+ y+
						" LeftX "+lx+" RightX "+rx+" LeftY "+ly+" RightY "+ry);
				System.out.println();
				System.out.print("My Neighbours are ");
				for(int i=0;i<neighbours.size();i++)
				{
					System.out.print(neighbours.get(i).name+" ");
				}
				System.out.println();
				System.out.print("My Files are ");
				for(int i=0;i<files.size();i++)
				{
					System.out.print(files.get(i).keyword+" ");
				}	
				System.out.println();
				return "SuccessFully left from system and space is Occupied by Peer: "+this.name +"  with IpAddress: "+this.ip;
			}
			else if(this.lx.equals(p.rx))
			{
			//	System.out.println( " h   e    elseif");
				this.lx=p.lx;
				this.x=(this.lx+this.rx)/2;
				this.occupyFiles(p);
				this.occupyNeighbours(p);
				try {
					s1.changeNode(p,this);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("I am expanded Horizontally and my Credentials are Name: "+name+"  X "+x +" Y "+ y+
						" LeftX "+lx+" RightX "+rx+" LeftY "+ly+" RightY "+ry);
				System.out.println();
				System.out.print("My Neighbours are ");
				for(int i=0;i<neighbours.size();i++)
				{
					System.out.print(neighbours.get(i).name+" ");
				}
				System.out.println();
				System.out.print("My Files are ");
				for(int i=0;i<files.size();i++)
				{
					System.out.print(files.get(i).keyword+" ");
				}	
				System.out.println();
				return "SuccessFully left from system and space is Occupied by Peer: "+this.name +"  with IpAddress: "+this.ip;
			}
		}
			return null;
		
	}
	
	/**
	 * This method helps to store the neighbors by the neighbouring peer that occupies the zone.
	 * 
	 * @param p
	 */
	public void occupyNeighbours(Peer1 p)
	{
		//remove the requested neighbour from my neighbourlist
		for(int i=0;i<neighbours.size();i++)
		{
			if(neighbours.get(i).ip.equals(p.ip))
			{
				neighbours.remove(i);
			}
		}
		// remove my object from my neighbour's neighbourlist
		for(int i=0;i<p.neighbours.size();i++)
		{
			if(p.neighbours.get(i).ip.equals(ip))
			{
				p.neighbours.remove(i);
			}
		}
		
		//add neighbours of p in my list which were not mine
		Boolean CheckN=false;
		Peer1 np;
		for(int j=0;j<p.neighbours.size();j++)
		{
			for(int i=0;i<neighbours.size();i++)
			{
				if(neighbours.get(i).ip.equals(p.neighbours.get(j).ip))
				{
					CheckN=true;
					break;
				}	
			}
			if(!CheckN)
			{	np=p.neighbours.get(j);
				neighbours.add(np);
			
			}
		}
		// remove the deleted peer and update the myself in my neighbours
		for(int i=0;i<neighbours.size();i++)
		{
			try{
			Registry reg = LocateRegistry.getRegistry(neighbours.get(i).ip, 12888);
			
		//	System.out.println("crossed registry");
			MiniServerInterface mc=(MiniServerInterface) reg.lookup("ms1");
			mc.upAndRm(this, p);
			
			
			}catch (NotBoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (RemoteException e) {
				e.printStackTrace();
				// TODO: handle exception
			}
		}
	}
	/**
	 * This method helps to store the files of peer by the neighboring peer which occupies the zone 
	 * 	
	 * @param p
	 */
	public void occupyFiles(Peer1 p)
	{
		for(int i=0;i<p.files.size();i++)
		{
			this.files.add(p.files.get(i));
		}
		
	}
	
	/**
	 * This method is invoked when user requests to leave the CAN system.
	 * @return
	 */
	public String removeMe()
	{
		String result=null;
		Boolean check=false;
		
		for(int i=0; i<neighbours.size();i++)
		{
			if((neighbours.get(i).lx.equals(lx))&&neighbours.get(i).rx.equals(rx))
			{
//				System.out.println("enter remove me if.............");
				String minSIp=neighbours.get(i).ip;
//				System.out.println("Searching route...... to peer "+neighbours.get(i).name+"  " +minSIp);
				Registry reg;
				try {
					reg = LocateRegistry.getRegistry(minSIp, 12888);
				
//				System.out.println("crossed registry");
				MiniServerInterface mc=(MiniServerInterface) reg.lookup("ms1");
				result=mc.overTake(this,"vertical");
				check=true;
				// code
				x=0.0;
				y=0.0;
				lx=0.0;
				ly=0.0;
				rx=0.0;
				ry=0.0;
				neighbours.clear();
				files.clear();
				
				}catch (NotBoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
		
				}
				catch (RemoteException e) {
					e.printStackTrace();
				
					// TODO: handle exception
				}
				break;
			}
			else if((neighbours.get(i).ly.equals(ly))&&neighbours.get(i).ry.equals(ry))
			{
//				System.out.println("enter remove me else.............");
				String minSIp=neighbours.get(i).ip;
//				System.out.println("Searching route...... to peer "+neighbours.get(i).name+"  " +minSIp);
				Registry reg;
				try {
					reg = LocateRegistry.getRegistry(minSIp, 12888);
				
//				System.out.println("crossed registry");
				MiniServerInterface mc=(MiniServerInterface) reg.lookup("ms1");
			result=	mc.overTake(this,"horizontal");
			check=true;
			x=0.0;
			y=0.0;
			lx=0.0;
			ly=0.0;
			rx=0.0;
			ry=0.0;
			neighbours.clear();
			files.clear();
				// code
				break;
				
				}catch (NotBoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				
				}
				catch (RemoteException e) {
					e.printStackTrace();
					
					// TODO: handle exception
				}
			}
	
		}
		
		
		if(check)
		{
			return result;
		}
		else
		{
			return "Can't leave from the System";
		}
		
	}
	/**
	 * THis method is used to search files in the peer.
	 * 
	 * @param file1
	 * 
	 */
	public Peer1 search(String file1)
	{
		File file= new File();
		file.keyword=file1;
		file.X=charAtEven(file1);
		file.Y=charAtOdd(file1);
		if(file.X>=this.lx && file.X<=this.rx && file.Y>=this.ly && file.Y<=this.ry)
		{
			Peer1 foundPeer=this;
			return foundPeer;
		}
		else
		{
			Peer1 minPeer= minDistance(file.X, file.Y);
			String minSIp=minPeer.ip;
			System.out.println("Searching route...... to peer "+minPeer.name+" "+minPeer.ip);
			Registry reg;
			try {
				reg = LocateRegistry.getRegistry(minSIp, 12888);
			
//			System.out.println("crossed registry");
			MiniServerInterface mc=(MiniServerInterface) reg.lookup("ms1");
			Peer1 foundPeer=mc.searchRouting(file);
			return foundPeer;
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
//				System.out.println("++++++++error++++++");
				return null;
			} catch (NotBoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
//				System.out.println("++++++++error++++++");
				return null;
			}	
			
		}
	}
	
	/**
	 * This method is invoked during routing for searching of file.
	 * 
	 * @param file
	 * 
	 */
	public Peer1 search(File file)
	{
		if(file.X>=this.lx && file.X<=this.rx && file.Y>=this.ly && file.Y<=this.ry)
		{	
			System.out.println("Got Request for File " +file.keyword);
			return this;
		}
		else
			{
			Peer1 minPeer= minDistance(file.X, file.Y);
			String minSIp=minPeer.ip;
			System.out.println("Searching route route...... to peer "+minPeer.name+" "+minPeer.ip);
			Registry reg;
			try {
				reg = LocateRegistry.getRegistry(minSIp, 12888);
		
//				System.out.println("crossed registry");
				MiniServerInterface mc=(MiniServerInterface) reg.lookup("ms1");
			Peer1 foundPeer1=mc.searchRouting(file);
			return foundPeer1;
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
//				System.out.println("++++++++error++++++");
				e.printStackTrace();
				return null;
			} catch (NotBoundException e) {
				// TODO Auto-generated catch block
//				System.out.println("++++++++error++++++");
				e.printStackTrace();
				return null;
			}	
		}
		
	}
	/**
	 * This method is used to insert file in peer during routing.
	 * 
	 * @param file
	 * 
	 */
	public String insert(File file) // while routing
	{
		
		Boolean foundFile=false;
		if(file.X>=this.lx && file.X<=this.rx && file.Y>=this.ly && file.Y<=this.ry)
		{	
			for(int i=0;i<this.files.size();i++)
			{
				if(this.files.get(i).keyword.equals(file.keyword))
				{
					foundFile=true;
					break;
				}
			}
			if(foundFile==false)
			{
				this.files.add(file);
				System.out.println("file inserted in the system : "+this.name);
				return file.keyword+" File inserted successfully in Peer: "+this.name;
			}else
			{
				System.out.println("File can't be inserted with same name");
				return file.keyword+" File already present in "+this.name+". Can't insert file with same name";
			}
		}
		else
		{
			Peer1 minPeer= minDistance(file.X, file.Y);
			String minSIp=minPeer.ip;
			System.out.println("Insertion route...... to peer "+minPeer.name+" "+minPeer.ip);
			Registry reg;
			try {
				reg = LocateRegistry.getRegistry(minSIp, 12888);
			
//			System.out.println("crossed registry");
			MiniServerInterface mc=(MiniServerInterface) reg.lookup("ms1");
			return mc.insertRouting(file);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (NotBoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}	
			
		}
		
		
	}
	/**
	 * This method is used to insert file in the system.
	 * 
	 * @param file1
	 * 
	 */
	public String insert(String file1) // in my own system
	{
		File file= new File();
		file.keyword=file1;
		file.X=charAtEven(file1);
		file.Y=charAtOdd(file1);
		Boolean found=false;
		if(file.X>=this.lx && file.X<=this.rx && file.Y>=this.ly && file.Y<=this.ry)
		{	
			
			for(int i=0;i<this.files.size();i++)
			{
				if(this.files.get(i).keyword.equals(file1))
				{
					found=true;
					break;
				}
			}
			if(found==false)
			{
				this.files.add(file);
				System.out.println();
				return file1+" File Succefully inserted in my system : "+this.name;
				//updateMyNeighbours(this);
			}	
			else
			{
				//System.out.println("File can't be inserted with same name");
				return file1+" File already present in my system. Can't Insert Duplicate File";
			}
		}
		else
		{ 
			Peer1 minPeer= minDistance(file.X, file.Y);
			String minSIp=minPeer.ip;
			System.out.println("Insertion route...... to peer "+minPeer.name+" "+minPeer.ip);
			Registry reg;
			try {
				reg = LocateRegistry.getRegistry(minSIp, 12888);
			
//			System.out.println("croosed registry");
			MiniServerInterface mc=(MiniServerInterface) reg.lookup("ms1");
			return mc.insertRouting(file);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (NotBoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}	
			
		}
	
	}
	/**
	 * This method calculates the random x coordinate for the file.
	 * 
	 * @param keyword
	 * @return x1
	 */
	public Double charAtOdd(String keyword)
	{
		Double x1=0.0;
		for(int i=0;i<keyword.length();i++)
		{
			if(i%2 !=0)
			{
				 x1+=(double)Character.getNumericValue(keyword.charAt(i));
				
			}
			
		}
		x1=x1%10;
	
		return x1;
	}
	/**
	 * This method calculates the y coordinate for the file.
	 * 
	 * @param keyword
	 * @return y1
	 */
	public Double charAtEven(String keyword)
	{
		Double y1=0.0;
		for(int i=0;i<keyword.length();i++)
		{
			if(i%2 ==0)
			{
				 y1+=(double)Character.getNumericValue(keyword.charAt(i));	
			}
		}
		y1=y1%10;
		return y1;
	}
	
	/**
	 * This method gives the credentials to the first node, and if not the first node
	 * calls the routing algorithm
	 * 
	 * @param p
	 * @param X
	 * @param Y
	 * @param sPeer1
	 */
	public void position(Peer1 p,Double X,Double Y,Peer1 sPeer1)
	{	// very 1st node
		this.bootNode=sPeer1;
//		System.out.println("entered position");
		if(p.ip.equals(bootNode.ip)) // 1st node 
		{	System.out.println("same");
			this.x=5.0;
			this.y=5.0;	
			this.lx=0.0;
			this.ly=0.0;
			this.rx=10.0;
			this.ry=10.0;
			
			try {
				s1.updateNode(this);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{	
			// other node
		Peer1 foundPeer=route(X,Y,bootNode);
		this.x=foundPeer.x;
		this.y=foundPeer.y;
		this.lx=foundPeer.lx;
		this.rx=foundPeer.rx;
		this.ly=foundPeer.ly;
		this.ry=foundPeer.ry;
		this.neighbours=foundPeer.neighbours;
		this.files= foundPeer.files;
		
		try {
			s1.updateNode(this);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		}
	}
	/**
	 * This method is invoked while routing to split the peer and give credentials to the new peer.
	 * 
	 * @param X
	 * @param Y
	 * @param p
	 * @return p,foundPeer new peer
	 */
	public Peer1 look(Double X,Double Y,Peer1 p)
	{	System.out.println();
		System.out.println("X "+X+" Y "+Y+" lx"+p.lx+" rx"+p.rx+" ly"+p.ly+ "  ry"+p.ry);
		if(X>=this.lx && X<=this.rx && Y>=this.ly && Y<=this.ry) // destination peer
		{
			if((this.rx-this.lx) >=(this.ry-this.ly))
			{// vertical break
//				System.out.println("vertical break");
				Double x2=(this.rx+this.lx)/2;
				if(X>x2)
				{
					System.out.println("vertical break..... new peer created on right side and me shifted to left side");
					p.lx=x2;
					p.ly=this.ly;
					p.rx=this.rx;	
					p.ry=this.ry;
					p.x=(p.lx+ p.rx)/2;
					p.y= (p.ly+p.ry)/2;
					this.rx=x2;
					this.x= (this.lx+this.rx)/2;
					this.y=(this.ly+this.ry)/2;
					
//					System.out.println("++++===++=++=+==+==+==++=++=++=+==++=+ ");
					p=this.updateFiles(p);
//					System.out.println("++++===++=++=+==+==+==++=++=++=+==++=+ ");
					
					
					this.addOneNeighnour(p);
					p.updateOthersNeighbours(p,this);
					this.updateMyNeighbours(this);
					
					
					p.addOneNeighnour(this);
					System.out.println(" My Credentials are Name: "+this.name+"  X "+this.x +" Y "+ this.y+
							" LeftX "+this.lx+" RightX "+this.rx+" LeftY "+this.ly+" RightY "+this.ry);
					System.out.println();
					System.out.print("My Neighbours are ");
					for(int i=0;i<this.neighbours.size();i++)
					{
						System.out.print(this.neighbours.get(i).name+" ");
					}
					System.out.println();
					System.out.print("My Files are ");
					for(int i=0;i<this.files.size();i++)
					{
						System.out.print(this.files.get(i).keyword+" ");
					}	
					System.out.println();
					System.out.println("Peer "+p.name+" Created");
					
				
					try {
						s1.updateNode(this);
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
					return p;
				}
				else
				{
					System.out.println("vertical break.... new peer created on left side and me shifted to right side ");
					p.lx=this.lx;
					p.ly=this.ly;
					p.rx=x2;	
					p.ry=this.ry;
					p.x=(p.rx+p.lx)/2;
					p.y=(p.ly+p.ry)/2;
					System.out.println("x of new peer "+p.x);
					System.out.println("y of new peer "+p.y);
					this.lx=x2;
					this.x= (this.lx+this.rx)/2;
					this.y=(this.ly+this.ry)/2;
					this.addOneNeighnour(p);
					p.updateOthersNeighbours(p,this);
					this.updateMyNeighbours(this);
//					System.out.println("++++===++=++=+==+==+==++=++=++=+==++=+ ");
					p=this.updateFiles(p);
//					System.out.println("++++===++=++=+==+==+==++=++=++=+==++=+ ");
					p.addOneNeighnour(this);
					System.out.println(" My Credentials are Name: "+this.name+"  X "+this.x +" Y "+ this.y+
							" LeftX "+this.lx+" RightX "+this.rx+" LeftY "+this.ly+" RightY "+this.ry);
					System.out.println();
					System.out.print("My Neighbours are ");
					for(int i=0;i<this.neighbours.size();i++)
					{
						System.out.print(this.neighbours.get(i).name+" ");
					}
					System.out.println();
					System.out.print("My Files are ");
					for(int i=0;i<this.files.size();i++)
					{
						System.out.print(this.files.get(i).keyword+" ");
					}	
					System.out.println();
					System.out.println("Peer "+p.name+" Created");
					
					try {
						s1.updateNode(this);
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return p;
				}
			}
			else
			{	// horizontal break
				System.out.println("horizontal break");
				Double y2=(this.ry+this.ly)/2;
				if(Y>y2) 
				{
					System.out.println("horizontal break...... new peer created on upper side and me shifted to lower part");
					p.lx=this.lx;
					p.ly=y2;
					p.rx=this.rx;
					p.ry=this.ry;
					p.x=(p.lx+p.rx)/2;
					p.y=(p.ly+p.ry)/2;
					this.ry=y2;
					this.x= (this.lx+this.rx)/2;
					this.y=(this.ly+this.ry)/2;
					this.addOneNeighnour(p);
					p.updateOthersNeighbours(p,this);
					this.updateMyNeighbours(this);
					
				//	System.out.println("++++===++=++=+==+==+==++=++=++=+==++=+ ");
					p=this.updateFiles(p);
				//	System.out.println("++++===++=++=+==+==+==++=++=++=+==++=+ ");
					
					p.addOneNeighnour(this);
					System.out.println(" My Credentials are Name: "+this.name+"  X "+this.x +" Y "+ this.y+
							" LeftX "+this.lx+" RightX "+this.rx+" LeftY "+this.ly+" RightY "+this.ry);
					System.out.println();
					System.out.print("My Neighbours are ");
					for(int i=0;i<this.neighbours.size();i++)
					{
						System.out.print(this.neighbours.get(i).name+" ");
					}
					System.out.println();
					System.out.print("My Files are ");
					for(int i=0;i<this.files.size();i++)
					{
						System.out.print(this.files.get(i).keyword+" ");
					}	
					System.out.println();
					System.out.println("Peer "+p.name+" Created");
					
					try {
						s1.updateNode(this);
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return p;		
				}
				else
				{
					System.out.println("horizontal break...... new peer created on lower side and me shifted to upper part");
					p.lx=this.lx;
					p.ly=this.ly;
					p.rx=this.rx;
					p.ry=y2;
					p.y=(p.ly+p.ry)/2;
					p.x=(p.lx+p.rx)/2;
					this.ly=y2;
					this.x=(this.lx+this.rx)/2;
					this.y=(this.ly+this.ry)/2;
					this.addOneNeighnour(p);
					p.updateOthersNeighbours(p,this);
					this.updateMyNeighbours(this);
					
				//	System.out.println("++++===++=++=+==+==+==++=++=++=+==++=+ ");
					p=this.updateFiles(p);
				//	System.out.println("++++===++=++=+==+==+==++=++=++=+==++=+ ");
					
					p.addOneNeighnour(this);
					System.out.println("My Credentials are Name: "+this.name+"  X "+this.x +" Y "+ this.y+
							" LeftX "+this.lx+" RightX "+this.rx+" LeftY "+this.ly+" RightY "+this.ry);
					System.out.println();
					System.out.print("Neighbours ");
					for(int i=0;i<this.neighbours.size();i++)
					{
						System.out.print(this.neighbours.get(i).name+" ");
					}
					System.out.println();
					System.out.print("Files");
					for(int i=0;i<this.files.size();i++)
					{
						System.out.print(this.files.get(i).keyword+" ");
					}	
					System.out.println();
					System.out.println("Peer "+p.name+" Created");
					
					try {
						s1.updateNode(this);
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}					
					return p;		
				}
			}		
		}
		// If the required area is not inside my zone , it further request to route to the nearest neigbhbouring peer.
		else
		{
			Peer1 foundPeer=routeAgain(X, Y, p);
			return foundPeer;
		}
	}
	/**
	 * This method is called to route the request to nearest neighbouring peer
	 * 
	 * @param X
	 * @param Y
	 * @param p
	 * @return foundPeer
	 */
	public Peer1 routeAgain( Double X,Double Y,Peer1 p)
	{
		
		
		Peer1 tempSPeer= minDistance(X, Y);
		System.out.println("Route Again to "+ tempSPeer.ip);
		try{
		
//		System.out.println("bind try");
		String minSIp=tempSPeer.ip;
	//System.out.println("Route again...... to "+tempSPeer.name+" "+tempSPeer.ip);
		Registry reg = LocateRegistry.getRegistry(minSIp, 12888);
	//	System.out.println("croosed registry");
		MiniServerInterface mc=(MiniServerInterface) reg.lookup("ms1");
		Peer1 foundPeer;
		foundPeer=mc.divideArea(X,Y,p);
		return foundPeer;
		}
		catch(RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}	
	/**
	 * This method calculates the minimum distance and returns the peer
	 * with minimum distance.
	 * 
	 * @param X
	 * @param Y
	 * @return tempSPeer
	 */
	public Peer1 minDistance(Double X,Double Y)
	{
		ArrayList<Double> distList= new ArrayList<Double>();
		for(int i=0;i<this.neighbours.size();i++)
		{	
			Double dist= Math.pow((X-this.neighbours.get(i).x), 2) + Math.pow((Y-this.neighbours.get(i).y), 2);
			distList.add(dist);
		}
		Double minDist=distList.get(0);
		System.out.println("size of list of neighnours of boot node"+distList.size());
		for(int i=0;i<distList.size();i++)
		{
			if(distList.get(i)<=minDist)
			{
				minDist=distList.get(i);
			}
		
		}
		System.out.println("found min distance "+minDist);
		Peer1 tempSPeer=null;
		for(int i=0;i<distList.size();i++)
		{
			if(minDist.equals(distList.get(i)))
			{
				tempSPeer= this.neighbours.get(i);
				System.out.println("comparison done "+minDist +" for neighbour"+tempSPeer.name);
				
			break;
			}
		
		}
		return tempSPeer;
		
	}
	/**
	 * This method returns the new peer which we will get after routing.
	 * 
	 * @param X
	 * @param Y
	 * @param BootNode
	 * @return foundPeer
	 */
	public Peer1 route( Double X,Double Y,Peer1 BootNode){
		try {
			//System.out.println("route bind try");
			String minSIp=BootNode.ip;
		//	System.out.println("Route to "+bootNode.name+" "+minSIp);
			Registry reg = LocateRegistry.getRegistry(minSIp, 12888);
			//System.out.println("croosed registry");
			MiniServerInterface mc=(MiniServerInterface) reg.lookup("ms1");
			//System.out.println("looking done in route");
			Peer1 foundPeer=mc.divideArea(X,Y,this);
			return foundPeer;
			
			//code 
			
		}
		catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}		
	}
	/**
	 * This method retrieves the IP Address of the local peer.
	 * 
	 * @return ipA
	 */
	public String getIpAddress()
	{ 	String ipA=null;
		try {
			ipA = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ipA;
	}
	/**
	 * This method generates random x and y coordinate of the peer which is going to be joined in the CAN system.
	 * 
	 * @return x
	 * @throws RemoteException
	 */
	public Double calculateCoordinate() throws RemoteException {
		Random rd= new Random();
		Double x = (double)rd.nextInt(10);
		return x;
	}
	
	/**
	 * This method adds the peer in the neighbour list of the peer.
	 * 
	 * @param peer
	 */
	public void addOneNeighnour(Peer1 peer)
	{
		neighbours.add(peer);
//		System.out.println("I am updated and Credentials are: "+name+"  "+x +" "+ y+" "+lx+" "+rx+" "+ly+" "+ry);
//		System.out.print("Neighbours  ");
//		for(int i=0;i<neighbours.size();i++)
//		{
//			System.out.print(neighbours.get(i).name+" ");
//		}
		try {
			s1.updateNode(this);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * This method updates and removes the peer while overtaking of the peer during leave.
	 * 
	 * @param p
	 */
	public void upAndAddNeighbour(Peer1 p)
	{
		Boolean bc=false;
		for(int i=0;i<neighbours.size();i++)
		{	
			// remove that particular peer which  need to update if it is present
			if(p.ip.equals(this.neighbours.get(i).ip))
			{
				bc=true;
				neighbours.remove(i);
			}
		}
		// add peer in the neighbours
		neighbours.add(p);
	}
	
	/**
	 * THis method removes the peer from the neighbor list.
	 * 
	 * @param p
	 */
	public void removeOneNeighbour(Peer1 p)
	{
		for(int i=0;i<neighbours.size();i++)
		{
			if(neighbours.get(i).ip.equals(p.ip))
			{
				neighbours.remove(i);
			}
		}
//		System.out.println("I am updated and Credentials are: "+name+"  "+x +" "+ y+" "+lx+" "+rx+" "+ly+" "+ry);
//		System.out.print("Neighbours  ");
//		for(int i=0;i<neighbours.size();i++)
//		{
//			System.out.print(neighbours.get(i).name+" ");
//		}
		try {
			s1.updateNode(this);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * This method fixes the meighbor list of the neighbor
	 * 
	 * @param p
	 */
	public void fixNeighbours(Peer1 p)
	{
		for(int i=0;i<neighbours.size();i++)
		{
			if(neighbours.get(i).ip.equals(p.ip))
			{
				neighbours.remove(i);
				neighbours.add(p);
			}
			
			
		}	
//		System.out.println("I am updated and Credentials are: "+name+"  "+x +" "+ y+" "+lx+" "+rx+" "+ly+" "+ry);
//		System.out.print("Neighbours  ");
//		for(int i=0;i<neighbours.size();i++)
//		{
//			System.out.print(neighbours.get(i).name+" ");
//		}
		
	}
	/**
	 * This method updates my neighbor list during creation  of new neighbor.
	 * 
	 * @param p
	 */
	public void updateMyNeighbours(Peer1 p)
	{
		//System.out.println("updateMyNeighbour");
		// fixing my area / credentials in other peers 
		for(int i=0;i<p.neighbours.size();i++)
		{

		//	System.out.println("bind try... update my neighbours");
			String minSIp=p.neighbours.get(i).ip;
			Registry reg;
			try {
				reg = LocateRegistry.getRegistry(minSIp, 12888);
			
			
			//	System.out.println("crossed registry for updateMyNeighbours to look "+p.name);
				MiniServerInterface mc=(MiniServerInterface) reg.lookup("ms1");
				mc.updateNeighbour(p, 3);
				
				
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				catch (NotBoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			
		}
		
		
		for(int i=0;i<p.neighbours.size();i++)
		{
			// for diagonals
//			System.out.println("chala ya nhi");
//			System.out.println("My points:"+p.lx+"  "+p.rx+" "+p.ly+" "+p.ry+"       " +p.neighbours.get(i).lx+
//					" "+p.neighbours.get(i).rx+"  "+p.neighbours.get(i).ly+" "+p.neighbours.get(i).ry);
			if((p.rx.equals(p.neighbours.get(i).lx)) && (p.ry.equals(p.neighbours.get(i).ly)) 
				||(p.lx.equals(p.neighbours.get(i).rx)) && (p.ry.equals(p.neighbours.get(i).ly))
				||(p.lx.equals(p.neighbours.get(i).rx)) && (p.ly.equals(p.neighbours.get(i).ry))
				||(p.rx.equals(p.neighbours.get(i).lx)) && (p.ly.equals(p.neighbours.get(i).ry)))
				{
				
					
					
				//	System.out.println("bind try... update my neighbours");
					String minSIp=p.neighbours.get(i).ip;
					Registry reg;
					try {
						reg = LocateRegistry.getRegistry(minSIp, 12888);
					
					
				//	System.out.println("crossed registry for updateMyNeighbours to look "+p.name);
					MiniServerInterface mc=(MiniServerInterface) reg.lookup("ms1");
					mc.updateNeighbour(p, 1);
					
					
					p.neighbours.remove(i);
					
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					catch (NotBoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				
				}
		}
		for(int i=0;i<p.neighbours.size();i++)
		{
			// for non adjacents
			if(((p.lx.compareTo(p.neighbours.get(i).rx))>0) ||((p.rx.compareTo(p.neighbours.get(i).lx))<0)
					||((p.ly.compareTo(p.neighbours.get(i).ry))>0) ||((p.ry.compareTo(p.neighbours.get(i).ly))<0))
				{
				System.out.println("bind try");
				String minSIp=p.neighbours.get(i).ip;
				Registry reg;
				try {
					reg = LocateRegistry.getRegistry(minSIp, 12888);
				
//				System.out.println("crossed registry");
//				System.out.println("crossed registry for updateMyNeighbours to look "+p.name);
				MiniServerInterface mc=(MiniServerInterface) reg.lookup("ms1");
				mc.updateNeighbour(p, 1);
				
					p.neighbours.remove(i);
				}
					catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
						catch (NotBoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				}	
		}
		
	}
	/**
	 * This method updates the list of new peer which is created after splitting the zone.
	 * 
	 * @param p1
	 * @param p2
	 */
	public void updateOthersNeighbours(Peer1 p1,Peer1 p2)
	{
		//System.out.println("updateOtherNeighbours");
		for(int i =0;i<p2.neighbours.size();i++)
		{
			
				if(!(p1.ip.equals(p2.neighbours.get(i).ip)))
				{	
					p1.neighbours.add(p2.neighbours.get(i));
				}
			
		}
		for(int i=0;i<p1.neighbours.size();i++)
		{
			// for diagonals
			if((p1.rx.equals(p1.neighbours.get(i).lx)) && (p1.ry.equals(p1.neighbours.get(i).ly)) 
				||(p1.lx.equals(p1.neighbours.get(i).rx)) && (p1.ry.equals(p1.neighbours.get(i).ly))
				||(p1.lx.equals(p1.neighbours.get(i).rx)) && (p1.ly.equals(p1.neighbours.get(i).ry))
				||(p1.rx.equals(p1.neighbours.get(i).lx)) && (p1.ly.equals(p1.neighbours.get(i).ry)))
				{
					System.out.println("removing"+p1.neighbours.get(i).name);
					
					p1.neighbours.remove(i);

				}
		}
		for(int i=0;i<p1.neighbours.size();i++)
		{
			// for non adjacents
			if(((p1.lx.compareTo(p1.neighbours.get(i).rx))>0) ||((p1.rx.compareTo(p1.neighbours.get(i).lx))<0)
					||((p1.ly.compareTo(p1.neighbours.get(i).ry))>0) ||((p1.ry.compareTo(p1.neighbours.get(i).ly))<0))
				{
					
	//			System.out.println("removing"+p1.neighbours.get(i).name);
					p1.neighbours.remove(i);
				}
					
			}
		for(int i=0;i<p1.neighbours.size();i++)
		{
			System.out.println("bind try");
			String minSIp=p1.neighbours.get(i).ip;
			
			Registry reg;
			try {
				reg = LocateRegistry.getRegistry(minSIp, 12888);
			MiniServerInterface mc=(MiniServerInterface) reg.lookup("ms1");
			mc.updateNeighbour(p1, 2);
			
		}
			catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				catch (NotBoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
	}
	/**
	 * This method updates the files of the peer.
	 * 
	 * @param p
	 * @return p
	 */
	public Peer1 updateFiles(Peer1 p)
	{
		for(int i=0;i<this.files.size();i++)
		{
			if(this.files.get(i).X>=this.lx && this.files.get(i).X<=this.rx && this.files.get(i).Y>=this.ly
					&& this.files.get(i).Y<=this.ry)
			{
				//everything is fine
			}
			else
			{
				p.files.add(this.files.get(i));
				
			//	System.out.println("+++++++++++++++++hahahahahah file added and removed as well");
				
				this.files.remove(i);
				i--;
			}
		}
		return p;
	}
}

	
	
